import  { useState } from 'react'

export const useCounter = (vaoloresInicales = 0) => {
    const [counter, setcounter] = useState(vaoloresInicales)
    
    const add = () => {
        setcounter(counter + 1)
    }

    const rest = () => {
        setcounter(counter - 1)
    }

    const reset = () => {
        setcounter(vaoloresInicales)
    }

    return [counter, add, rest, reset]
}
