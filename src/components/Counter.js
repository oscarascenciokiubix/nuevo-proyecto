import React from 'react'
import { useCounter } from '../hooks/useCounter'

export const Counter = () => {
    const [counter, add, rest, reset] = useCounter(10)
    return (
        <div>
            <h2>{counter}</h2>
            <button onClick={add}>+1</button>
            <button onClick={rest}>-1</button>
            <button onClick={reset}>reset</button>
        </div>
    )
}
